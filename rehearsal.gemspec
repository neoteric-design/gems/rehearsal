require File.expand_path('lib/rehearsal/version', __dir__)

Gem::Specification.new do |gem|
  gem.authors       = ['Joe Sak']
  gem.email         = ['joe@joesak.com']
  gem.description   = 'Quickly add Staging Env. HTTP basic auth to your project'
  gem.summary       = 'Easy HTTP Basic Auth for Staging Rails Apps'
  gem.homepage      = ''

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = 'rehearsal'
  gem.require_paths = %w[lib vendor]
  gem.version       = Rehearsal::VERSION
end
